import {schema} from "prosemirror-schema-basic"
import {Schema} from "prosemirror-model"
import {tableNodes} from "prosemirror-tables"
import testBuilder from "prosemirror-test-builder"

import {addTableSections} from "../src/schema"

const tableSchema = new Schema({
    nodes: schema.spec.nodes.append(addTableSections(tableNodes({
        tableGroup: "block",
        cellContent: "block+"
    }))),
    marks: schema.spec.marks
})

const {doc, table, row_head, row_body, row_foot, cell, paragraph} = testBuilder.builders(tableSchema, {
    cell: {nodeType: "table_cell"},
    row_head: {nodeType: "table_row", section: "head"},
    row_body: {nodeType: "table_row", section: "body"},
    row_foot: {nodeType: "table_row", section: "foot"}
})

export const headRow = text => row_head(cell(paragraph(text)))
export const bodyRow = text => row_body(cell(paragraph(text)))
export const footRow = text => row_foot(cell(paragraph(text)))

export {doc, table}
