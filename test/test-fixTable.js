import {EditorState} from "prosemirror-state"
import ist from "ist"

import {doc, table, headRow, bodyRow, footRow} from "./base"
import {fixTable} from "../src/plugin"


function test(beforeTable, afterTable, options = {}) {
    const state = EditorState.create({doc: doc(beforeTable)}),
        tr = fixTable(state, beforeTable, 0, null, options)
    ist(JSON.stringify(tr.doc.nodeAt(0).toJSON()), JSON.stringify(afterTable.toJSON()))
}


describe("fixTable", () => {
    it("doesn't touch correct tables without headers or footers", () => {
        test(
            table(bodyRow("A"), bodyRow("B"), bodyRow("C")),
            table(bodyRow("A"), bodyRow("B"), bodyRow("C"))
        )
    })

    it("doesn't touch correct tables with headrs or footers", () => {
        test(
            table(headRow("A"), bodyRow("B"), footRow("C")),
            table(headRow("A"), bodyRow("B"), footRow("C"))
        )
    })

    it("converts headRow after bodyRow", () => {
        test(
            table(bodyRow("A"), headRow("B"), footRow("C")),
            table(bodyRow("A"), bodyRow("B"), footRow("C"))
        )
    })

    it("converts bodyRow before footRow", () => {
        test(
            table(headRow("A"), footRow("B"), bodyRow("C")),
            table(headRow("A"), footRow("B"), footRow("C"))
        )
    })

    it("allows only one headRow if option maxOneHead utilized", () => {
        test(
            table(headRow("A"), headRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            table(headRow("A"), bodyRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            {maxOneHead: true}
        )
    })

    it("allows only one footRow if option maxOneFoot utilized", () => {
        test(
            table(headRow("A"), headRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            table(headRow("A"), headRow("B"), bodyRow("C"), bodyRow("D"), footRow("E")),
            {maxOneFoot: true}
        )
    })

    it("combines maxOneFoot and maxOneHead options", () => {
        test(
            table(headRow("A"), headRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            table(headRow("A"), bodyRow("B"), bodyRow("C"), bodyRow("D"), footRow("E")),
            {maxOneHead: true, maxOneFoot: true}
        )
    })

    it("ensures one headRow if option minOneHead utilized", () => {
        test(
            table(bodyRow("A"), bodyRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            table(headRow("A"), bodyRow("B"), bodyRow("C"), footRow("D"), footRow("E")),
            {minOneHead: true}
        )
    })

    it("ensures one footRow if option minOneFoot utilized", () => {
        test(
            table(headRow("A"), headRow("B"), bodyRow("C"), bodyRow("D"), bodyRow("E")),
            table(headRow("A"), headRow("B"), bodyRow("C"), bodyRow("D"), footRow("E")),
            {minOneFoot: true}
        )
    })

    it("combines minOneFoot and minOneHead options", () => {
        test(
            table(bodyRow("A"), bodyRow("B"), bodyRow("C"), bodyRow("D"), bodyRow("E")),
            table(headRow("A"), bodyRow("B"), bodyRow("C"), bodyRow("D"), footRow("E")),
            {minOneHead: true, minOneFoot: true}
        )
    })

})
