import {
    isInTable,
    TableMap,
    selectionCell,
    CellSelection
} from "prosemirror-tables"

// Copied from prosemirror-tables as it's not being exported.
function selectedRect(state) {
    const sel = state.selection,
        $pos = selectionCell(state)
    const table = $pos.node(-1),
        tableStart = $pos.start(-1),
        map = TableMap.get(table)
    let rect
    if (sel instanceof CellSelection)
        rect = map.rectBetween(sel.$anchorCell.pos - tableStart, sel.$headCell.pos - tableStart)
    else
        rect = map.findCell($pos.pos - tableStart)
    rect.tableStart = tableStart
    rect.map = map
    rect.table = table
    return rect
}

function toggleRowSection(section, options) {
    return function(state, dispatch) {
        if (!isInTable(state)) return false
        const rect = selectedRect(state),
            rows = Array(rect.bottom - rect.top).fill().map((_, index) => rect.table.child(index + rect.top)),
            newSection = rows.find(row => row.attrs.section === section) ? 'body' : section
        if (newSection === 'head') {
            const aboveRows = Array(rect.top).fill().map((_, index) => rect.table.child(index))
            if (aboveRows.find(row => row.attrs.section !== 'head')) {
                // There are non-headers above this one.
                return false
            } else if (rect.bottom > 1 && options.maxOneHead) {
                // Trying to make more than one row a header row not allowed.
                return false
            }
        } else if (newSection === 'body') {
            if (options.minOneHead && rect.top === 0) {
                // Trying to turn the top row into a body row.
                return false
            } else if (options.minOneFoot && rect.bottom === rect.table.childCount) {
                // Trying to turn the bottom row into a body row.
                return false
            }
        } else if (newSection === 'foot') {
            const belowRows = Array(rect.table.childCount - rect.bottom).fill().map((_, index) => rect.table.child(index + rect.bottom))
            if (belowRows.find(row => row.attrs.section !== 'foot')) {
                // There are non-footers belows this one.
                return false
            } else if (rect.top < rect.table.childCount - 1 && options.maxOneFoot) {
                // Trying to make more than one row a footer row not allowed.
                return false
            }
        }
        if (dispatch) {
            const tr = state.tr
            let rowPos = Array(rect.top).fill().reduce((offset, _, index) => offset + rect.table.child(index).nodeSize, rect.tableStart)
            rows.forEach(row => {
                tr.setNodeMarkup(rowPos, null, Object.assign({}, row.attrs, {
                    section: newSection
                }))
                rowPos += row.nodeSize
            })
            dispatch(tr)
        }
        return true
    }
}

export function toggleRowHeader(options = {}) {
    return toggleRowSection("head", options)
}
export function toggleRowFooter(options = {}) {
    return toggleRowSection("foot", options)
}
