import { EditorState, Plugin, Transaction } from 'prosemirror-state';
import { Schema } from 'prosemirror-model';
import { TableNodes } from 'prosemirror-tables';

interface Options {
    minOneHead?: boolean;
    minOneFoot?: boolean;
    maxOneHead?: boolean;
    maxOneFoot?: boolean;
}

// plugin.js

export function tableSections(options?: Options) : Plugin;
export function fixTableSections<S extends Schema = any>(state: EditorState<S>, options?: Options) : Transaction<S> | undefined;

// schema.js

export function addTableSections(tableNodes: TableNodes) : TableNodes;

// commands.js

export function toggleRowHeader(options?: Options) : <S extends Schema = any>(state: EditorState<S>, dispatch?: (tr: Transaction<S>) => void) => boolean;
export function toggleRowFooter(options?: Options) : <S extends Schema = any>(state: EditorState<S>, dispatch?: (tr: Transaction<S>) => void) => boolean;
